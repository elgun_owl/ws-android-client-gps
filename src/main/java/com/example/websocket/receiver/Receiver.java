package com.example.websocket.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.websocket.model.WebSocketModel;

import okhttp3.OkHttpClient;
import okhttp3.Request;

public class Receiver extends BroadcastReceiver {

    private static OkHttpClient client;
    private WebSocketModel listener;

    public Receiver() {
        listener = new WebSocketModel();
        client = new OkHttpClient();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (isOnline(context)) {
            WebSocketModel.isOnline = true;
            System.out.println("On Recevice Start");
            initConnection();
        } else {
            WebSocketModel.isOnline = false;
            System.out.println("On Recevice Stop");
            client.connectionPool().evictAll();
        }
    }

    private boolean isOnline(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            return (netInfo != null && netInfo.isConnected());

        } catch (NullPointerException e) {
            e.printStackTrace();
            return false;
        }
    }
    public static void initConnection() {
        Request request = new Request.Builder().url("ws://192.168.1.182:8080/hi").build();
        System.out.println(request);
        WebSocketModel listener = new WebSocketModel();
        client.newWebSocket(request, listener);
    }
}

