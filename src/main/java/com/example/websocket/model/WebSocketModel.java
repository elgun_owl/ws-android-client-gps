package com.example.websocket.model;

import com.example.websocket.receiver.Receiver;
import org.jetbrains.annotations.NotNull;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;

public final class WebSocketModel extends WebSocketListener {
    public static Boolean isOnline;
    private static WebSocket webSocketThis;

    @Override
    public void onOpen(WebSocket webSocket, Response response) {
        System.out.println("Open connection");
        webSocketThis = webSocket;
        webSocket.send("Hello, it's Elgun !");
    }

    @Override
    public void onMessage(@NotNull WebSocket webSocket, @NotNull String text) {
        System.out.println(text);
    }

    @Override
    public void onClosing(WebSocket webSocket, int code, String reason) {
        System.out.println("Close connection");
    }

    @Override
    public void onFailure(WebSocket webSocket, Throwable t, Response response) {
        System.out.println("Fail: + " + t);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex){
            ex.printStackTrace();
        }
        if (isOnline ==true) {
          Receiver.initConnection();
        }
    }

    public static void send(){
        webSocketThis.send("Heloooo");
    }
}